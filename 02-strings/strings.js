reversString = () => {
  const value = document.getElementById("task01").value;
  const reverse = value
    .split("")
    .reverse()
    .join("");
  document.getElementById("results01").innerHTML = reverse;
};

checkBrackets = () => {
  let opened = 0;
  let closed = 0;

  const value = document.getElementById("task02").value;

  value
    .split("")
    .map(char => (char === ")" && opened++) || (char === "(" && closed++));

  if (!((opened || closed) % 2)) {
    document.getElementById("results02").innerHTML = "верно";
  } else {
    document.getElementById("results02").innerHTML = "не верно";
  }
};

searhString = () => {
  const value = document.getElementById("task03_01").value;
  const searh = document.getElementById("task03_02").value;
  const Reg = new RegExp(searh, "g");

  const result = Array.from(value.toLowerCase().matchAll(Reg)).length;

  document.getElementById("results03").innerHTML = result;
};
