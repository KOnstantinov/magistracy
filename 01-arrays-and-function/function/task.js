const numbers = {
  "1": "One",
  "2": "Two",
  "3": "Three",
  "4": "Four",
  "5": "Five",
  "6": "Six",
  "7": "Seven",
  "8": "Eight",
  "9": "Nine",
  "0": "Zero"
};

lastNumber = () => {
  const value = document.getElementById("task01").value;
  document.getElementById("results01").innerHTML =
    numbers[value[value.length - 1]];
};

reversNumber = () => {
  const value = document.getElementById("task02").value;
  const reverse = value
    .split("")
    .reverse()
    .join("");
  document.getElementById("results02").innerHTML = reverse;
};
